package com.howieisawesome.digitalairstrike.dollabillsyall.misc;

import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinDetermination;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.EqualsAndHashCodeMatchRule;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.Test;

import java.util.List;

public class PojoTest {
    private static final String POJO_PACKAGES[] = {
            "com.howieisawesome.digitalairstrike.dollabillsyall.entity",
            "com.howieisawesome.digitalairstrike.dollabillsyall.endpoint.abetternamegoeshere"
    };

    private static final String ENUM_PACKAGES[] = {
            "com.howieisawesome.digitalairstrike.dollabillsyall.entity"
    };

    @Test
    void testPojos() {
        Validator validator = ValidatorBuilder.create()
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                .with(new SetterTester())
                .with(new GetterTester())
                .with(new EqualsAndHashCodeMatchRule())
                .build();

        for(String currentPackage : POJO_PACKAGES) {
            validator.validate(PojoClassFactory.getPojoClassesRecursively(currentPackage, pojoClass -> {
                // we ignore CoinDetermination because it hides its internal map
                return !pojoClass.isEnum() && !pojoClass.getClazz().equals(CoinDetermination.class);
            }));
        }
    }

    @Test
    void testEnums() {
        Validator validator = ValidatorBuilder.create()
                .with(new GetterMustExistRule())
                .with(new GetterTester())
                .with(new EqualsAndHashCodeMatchRule())
                .build();

        for(String currentPackage : ENUM_PACKAGES) {
            validator.validate(PojoClassFactory.getPojoClassesRecursively(currentPackage, pojoClass -> pojoClass.isEnum()));
        }
    }
}
