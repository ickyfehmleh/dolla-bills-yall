package com.howieisawesome.digitalairstrike.dollabillsyall.figurerouter;

import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinDetermination;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoinDenominationStrategyTest {
    private CoinDenominationStrategy strategy;

    @BeforeEach
    void setUp() {
        strategy = new CoinDenominationStrategy(); // make sure we're not maintaining any state
    }

    @Test
    void determineCoinsForAmount() {
        final CoinDetermination rv = strategy.determineCoinsForAmount(5.92);
        assertNotNull(rv);
        assertEquals(5, rv.getCoinsUsed(CoinType.SILVER_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.HALF_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.QUARTER));
        assertEquals(1, rv.getCoinsUsed(CoinType.DIME));
        assertEquals(1, rv.getCoinsUsed(CoinType.NICKEL));
        assertEquals(2, rv.getCoinsUsed(CoinType.PENNY));
    }

    @Test
    void determineOtherCoinsForADifferentAmount() {
        final CoinDetermination rv = strategy.determineCoinsForAmount(5.67);
        assertNotNull(rv);
        assertEquals(5, rv.getCoinsUsed(CoinType.SILVER_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.HALF_DOLLAR));
        assertEquals(0, rv.getCoinsUsed(CoinType.QUARTER));
        assertEquals(1, rv.getCoinsUsed(CoinType.DIME));
        assertEquals(1, rv.getCoinsUsed(CoinType.NICKEL));
        assertEquals(2, rv.getCoinsUsed(CoinType.PENNY));
    }

    @Test
    void determineStillOtherCoinsForYetAnotherAmount() {
        final CoinDetermination rv = strategy.determineCoinsForAmount(17.76);
        assertNotNull(rv);
        assertEquals(17, rv.getCoinsUsed(CoinType.SILVER_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.HALF_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.QUARTER));
        assertEquals(0, rv.getCoinsUsed(CoinType.DIME));
        assertEquals(0, rv.getCoinsUsed(CoinType.NICKEL));
        assertEquals(1, rv.getCoinsUsed(CoinType.PENNY));
    }

    @Test
    void shhh() {
        final CoinDetermination rv = strategy.determineCoinsForAmount(143.805);
        assertNotNull(rv);
        assertEquals(143, rv.getCoinsUsed(CoinType.SILVER_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.HALF_DOLLAR));
        assertEquals(1, rv.getCoinsUsed(CoinType.QUARTER));
        assertEquals(0, rv.getCoinsUsed(CoinType.DIME));
        assertEquals(1, rv.getCoinsUsed(CoinType.NICKEL));
        assertEquals(0, rv.getCoinsUsed(CoinType.PENNY));
    }
}
