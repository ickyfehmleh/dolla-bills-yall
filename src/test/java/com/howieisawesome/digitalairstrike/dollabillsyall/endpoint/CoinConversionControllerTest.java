package com.howieisawesome.digitalairstrike.dollabillsyall.endpoint;

import com.howieisawesome.digitalairstrike.dollabillsyall.endpoint.abetternamegoeshere.CoinResponse;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinDetermination;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinType;
import com.howieisawesome.digitalairstrike.dollabillsyall.figurerouter.CoinDenominationStrategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CoinConversionControllerTest {

    @Mock
    private CoinDenominationStrategy strategy;

    @InjectMocks
    private CoinConversionController conversionController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void determineCoins() {
        final double inputAmount = 5.56;
        final long expectedSilverDollars = 1L;
        final long expectedHalfDollars = 2L;
        final long expectedQuarters = 3L;
        final long expectedDimes = 4L;
        final long expectedNickels = 5L;
        final long expectedPennies = 6L;
        CoinDetermination determination = mock(CoinDetermination.class);
        when(strategy.determineCoinsForAmount(anyDouble())).thenReturn(determination);

        when(determination.getCoinsUsed(eq(CoinType.SILVER_DOLLAR))).thenReturn(expectedSilverDollars);
        when(determination.getCoinsUsed(eq(CoinType.HALF_DOLLAR))).thenReturn(expectedHalfDollars);
        when(determination.getCoinsUsed(eq(CoinType.QUARTER))).thenReturn(expectedQuarters);
        when(determination.getCoinsUsed(eq(CoinType.DIME))).thenReturn(expectedDimes);
        when(determination.getCoinsUsed(eq(CoinType.NICKEL))).thenReturn(expectedNickels);
        when(determination.getCoinsUsed(eq(CoinType.PENNY))).thenReturn(expectedPennies);

        final CoinResponse rv = conversionController.determineCoins(inputAmount);

        assertNotNull(rv);

        assertEquals(expectedSilverDollars, rv.getSilverDollars());
        assertEquals(expectedHalfDollars, rv.getHalfDollars());
        assertEquals(expectedQuarters, rv.getQuarters());
        assertEquals(expectedDimes, rv.getDimes());
        assertEquals(expectedNickels, rv.getNickels());
        assertEquals(expectedPennies, rv.getPennies());

        // make sure the determination was used for every currency type we have
        for(CoinType currentType : CoinType.values()) {
            verify(determination, atLeastOnce()).getCoinsUsed(eq(currentType));
        }
    }

    @Test
    void handleErrorResponses() {
        final String exceptionMessage = "wut";
        final ResponseEntity<String> rv = conversionController.handleErrorResponses(new IllegalArgumentException(exceptionMessage));

        assertNotNull(rv);
        assertNotNull(rv.getBody());
        assertEquals(exceptionMessage, rv.getBody());
        assertEquals(HttpStatus.I_AM_A_TEAPOT, rv.getStatusCode());
    }

    @Test
    void handleErrorResponsesNullException() {
        final ResponseEntity<String> rv = conversionController.handleErrorResponses(null);

        assertNotNull(rv);
        assertNotNull(rv.getBody());
        assertEquals(HttpStatus.I_AM_A_TEAPOT, rv.getStatusCode());
    }
}
