package com.howieisawesome.digitalairstrike.dollabillsyall.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoinDeterminationTest {

    @Test
    void allTheThings() {
        CoinDetermination determination = new CoinDetermination();
        determination.setCoinsUsed(CoinType.PENNY, 100);

        assertEquals(100, determination.getCoinsUsed(CoinType.PENNY));
        assertEquals(0, determination.getCoinsUsed(CoinType.NICKEL));
    }
}
