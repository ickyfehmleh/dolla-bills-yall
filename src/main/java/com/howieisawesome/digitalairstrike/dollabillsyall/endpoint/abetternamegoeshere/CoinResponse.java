package com.howieisawesome.digitalairstrike.dollabillsyall.endpoint.abetternamegoeshere;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * Holds the to-be-jsonified response for the Controller
 *
 * There are three things that are really difficult in programming:
 * guaranteed single delivery, naming things, expiring caches, and guaranteed single delivery
 */
@Data
public class CoinResponse {
    private long silverDollars;
    private long halfDollars;
    private long quarters;
    private long dimes;
    private long nickels;
    private long pennies;

    @JsonIgnore
    private long taxes; // dont try this at home
}
