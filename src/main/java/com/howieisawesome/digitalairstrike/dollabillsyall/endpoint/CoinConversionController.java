package com.howieisawesome.digitalairstrike.dollabillsyall.endpoint;

import com.howieisawesome.digitalairstrike.dollabillsyall.endpoint.abetternamegoeshere.CoinResponse;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinDetermination;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinType;
import com.howieisawesome.digitalairstrike.dollabillsyall.figurerouter.CoinDenominationStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controls coin conversion correlating cost, count
 *
 * Bonus points for alliteration?
 */
@RestController
public class CoinConversionController {
    private static final Log LOGGER = LogFactory.getLog(CoinConversionController.class);

    // im not a huge fan of @Autowired fields, nor is Spring, at least according to Intelli-J who rarely lies to me
    // plus now things can be final.
    private final CoinDenominationStrategy coinDenominationStrategy;

    // i like providing everything in the constructor because the class is useless without its dependencies
    public CoinConversionController(CoinDenominationStrategy coinDenominationStrategy) {
        this.coinDenominationStrategy = coinDenominationStrategy;
    }

    @CrossOrigin
    @GetMapping({"/","/convert"})
    public CoinResponse determineCoins(@RequestParam("amount")final double amount) {
        final CoinResponse rv = new CoinResponse();
        final CoinDetermination determination = coinDenominationStrategy.determineCoinsForAmount(amount);

        // map things; this could go into the service layer or yet another layer could be
        // introduced, usually called the facade but sometimes also called "more code to test"
        rv.setSilverDollars(determination.getCoinsUsed(CoinType.SILVER_DOLLAR));
        rv.setHalfDollars(determination.getCoinsUsed(CoinType.HALF_DOLLAR));
        rv.setQuarters(determination.getCoinsUsed(CoinType.QUARTER));
        rv.setDimes(determination.getCoinsUsed(CoinType.DIME));
        rv.setNickels(determination.getCoinsUsed(CoinType.NICKEL));
        rv.setPennies(determination.getCoinsUsed(CoinType.PENNY));
        return rv;
    }

    @ExceptionHandler
    public ResponseEntity<String> handleErrorResponses(Exception e) {
        String body = "Something dun goofed";
        final ResponseEntity.BodyBuilder rv = ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT); // short and stout

        if(e != null) {
            LOGGER.error("Error handled", e);
            body = e.getMessage(); // showing technical messages to end users rarely ends well
        }

        return rv.body(body);
    }
}
