package com.howieisawesome.digitalairstrike.dollabillsyall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.howieisawesome.digitalairstrike.dollabillsyall.util.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@SpringBootApplication
@EnableCaching
public class DollaBillsYallApplication {

    public static void main(String[] args) {
        SpringApplication.run(DollaBillsYallApplication.class, args);
    }

    // usually bean definitions would be in a *Config class, but i wont tell if you wont
    @Bean
    public ObjectMapper objectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .indentOutput(true) // who doesnt like being pretty?
                .build();
    }

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(Constants.CONVERSION_CACHE_NAME);
    }
}
