package com.howieisawesome.digitalairstrike.dollabillsyall.figurerouter;

import com.howieisawesome.digitalairstrike.dollabillsyall.util.Constants;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinDetermination;
import com.howieisawesome.digitalairstrike.dollabillsyall.entity.CoinType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Given an amount, determine how many coins it takes to reach that amount with none left over just trust me
 */
@Component
public class CoinDenominationStrategy {
    private static final Log LOGGER = LogFactory.getLog(CoinDenominationStrategy.class);

    @Cacheable(Constants.CONVERSION_CACHE_NAME)
    public CoinDetermination determineCoinsForAmount(double amount) {
        LOGGER.info(String.format("Determining coins for amount [%.2f]", amount));

        final CoinDetermination rv = new CoinDetermination();
        long remainder = (long)(amount*100); // follow michael bolton's advice re:decimals

        for(CoinType currentType : CoinType.values()) {
            if(remainder >= currentType.getAmount()) {
                long coinsUsed = remainder / currentType.getAmount();
                remainder = remainder % currentType.getAmount();

                rv.setCoinsUsed(currentType, coinsUsed);
                LOGGER.debug(String.format("Currency [%s] will use [%d] coins for [%.2f]", currentType, coinsUsed, amount ));
            }
        }

        if(remainder > 0) {
            // TODO talk to Gus Gorman about refactoring this
        }

        return rv;
    }
}
