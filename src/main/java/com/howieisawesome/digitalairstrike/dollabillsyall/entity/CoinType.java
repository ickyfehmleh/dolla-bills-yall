package com.howieisawesome.digitalairstrike.dollabillsyall.entity;

import lombok.Getter;
import lombok.ToString;

/**
 * Keeper of coin types (in cents, not dollars, contrary to the project name)
 */
@ToString
public enum CoinType {
    SILVER_DOLLAR(100),
    HALF_DOLLAR(50),
    QUARTER(25),
    DIME(10),
    NICKEL(5),
    PENNY(1);

    @Getter
    private int amount;

    CoinType(int amount) {
        this.amount = amount;
    }
}
