package com.howieisawesome.digitalairstrike.dollabillsyall.entity;

import java.util.EnumMap;

/**
 * Important things are determined here, just you watch.
 *
 * This could be eliminated entirely and the Strategy could simply populate the Response directly, but that doesnt
 * feel right AND messes up my nice for() loop in the Strategy.  Also see note about the Facade in the Controller.
 */
public class CoinDetermination {
    private EnumMap<CoinType,Long> results = new EnumMap<>(CoinType.class);

    public void setCoinsUsed(CoinType coinType, long coinsUsed) {
        results.put(coinType, coinsUsed);
    }

    public long getCoinsUsed(CoinType coinType) {
        return results.getOrDefault(coinType, 0L);
    }
}
