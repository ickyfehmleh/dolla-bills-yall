package com.howieisawesome.digitalairstrike.dollabillsyall.util;

public class Constants {

    public final static String CONVERSION_CACHE_NAME = "conversionCache";

    private Constants() {
        // dont instantiate a Constants class you monster
    }
}
